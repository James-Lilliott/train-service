<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;


/**
 * Class TrainController
 *
 * @author JL
 * @package App\Http\Controllers
 */
class TrainController extends Controller
{
    /**
     * GuzzleHTTP Client
     *
     * @author JL
     * @var $client
     */
    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://transportapi.com/v3/uk/',
            'timeout'  => 2.0,
        ]);
    }

    private function callTransportApi(string $method, string $uri, array $payload = [])
    {
        try{
            $data = $this->client->request($method, $uri, $payload);
        } catch (exception $e){
            return $e;
        }

        return $data;
    }

    public function getTrainTimes()
    {
        $data = $this->callTransportApi('GET',
            'train/station/BMC/live.json',
            [
                'query' => [
                    'app_id' => env('TRANSPORT_API_ID'),
                    'app_key' => env('TRANSPORT_API_KEY')
                ]
            ]
        );

        dd(json_decode($data->getBody()));
    }
}
